import { createGlobalStyle } from 'styled-components';
import { accent } from 'constants/theme';
import siteConfig from '../site-config';

export default createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Fira|Fira+Mono');

  html, body, div, span, applet, object, iframe,
  h1, h2, h3, h4, h5, h6, p, blockquote, pre,
  a, abbr, acronym, address, big, cite, code,
  del, dfn, em, img, ins, kbd, q, s, samp,
  small, strike, strong, tt, var,
  b, u, i, center,
  dl, dt, dd, ol, ul, li,
  fieldset, form, label, legend,
  table, caption, tbody, tfoot, thead, tr, th, td,
  article, aside, canvas, details, embed,
  figure, figcaption, footer, header, hgroup,
  menu, nav, output, ruby, section, summary,
  time, mark, audio, video {
    margin: 0;
    padding: 0;
    border: 0;
    vertical-align: baseline;
  }

  h2 {
    font-size: 2.5rem;
    font-weight: 400;
    margin-bottom: 1rem;
    border-bottom: 1px solid #fff;
    padding-bottom: 5px;
  }

  h2::after {
    content: "";
    display: block;
    border-bottom: 1px solid #000;
    width 50%;
    position: relative;
    bottom: -6px;
  }

  h3 {
    font-size: 1.8rem;
    font-weight: 400;
    margin-bottom: 1rem;
    border-bottom: 1px solid #fff;
    padding-bottom: 5px;
  }
  
  h3::after {
    content: "";
    display: block;
    border-bottom: 1px solid #000;
    width 25%;
    position: relative;
    bottom: -6px;
  }

  /* HTML5 display-role reset for older browsers */
  article, aside, details, figcaption, figure,
  footer, header, hgroup, menu, nav, section {
    display: block;
  }

  html {
    font-size: 62.5%;
  }

  body {
    font-family: "Fira",-apple-system,BlinkMacSystemFont,"Segoe UI",Helvetica,Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
    line-height: 1.2;
    font-size: 1.6rem;
    color: #000;
    /* background-color: #fff; */
    background-image: url("${siteConfig.siteUrl}/noise.png");
    -webkit-text-size-adjust: 100%;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    text-rendering: optimizeLegibility;
    -webkit-font-feature-settings: "pnum";
    font-feature-settings: "pnum";
    font-variant-numeric: proportional-nums;
  }

  ol, ul {
    list-style: none;
    margin-bottom: 2rem;
  }

  blockquote, q {
    quotes: none;
  }

  blockquote:before, blockquote:after,
  q:before, q:after {
    content: '';
    content: none;
  }

  table {
    border-collapse: collapse;
    border-spacing: 0;
  }

  a {
    color: ${accent};
  }

  pre {
    display: block;
    padding: 2rem;
    margin-top: 2rem;
    margin-bottom: 2rem;
    overflow: auto;
    font-size: 85%;
    line-height: 1.2;
    border-radius: 5px;
    color: ${accent};
    border: 1px solid #ddd;
    font-family: "Fira Mono","SFMono-Regular",Consolas,Menlo,Courier,monospace;
  }

  video {
    max-width: 100%;
  }

  p {
    margin-bottom: 2rem;
  }
`;
