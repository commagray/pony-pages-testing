import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'gatsby';
import Layout from 'components/layout';
import Box from 'components/box';
import Head from 'components/head';

const Services = ({ data }) => (
  <Layout>
    <Head pageTitle={data.servicesJson.title} />
    <Box>
      <div
        dangerouslySetInnerHTML={{
          __html: data.servicesJson.content.childMarkdownRemark.html,
        }}
      />
    </Box>
  </Layout>
);

Services.propTypes = {
  data: PropTypes.object.isRequired,
};

export default Services;

export const query = graphql`
  query ServicesQuery {
    servicesJson {
      title
      content {
        childMarkdownRemark {
          html
        }
      }
    }
  }
`;
