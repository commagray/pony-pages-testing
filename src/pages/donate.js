import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'gatsby';
import Layout from 'components/layout';
import Box from 'components/box';
import Head from 'components/head';

const Donate = ({ data }) => (
  <Layout>
    <Head pageTitle={data.donateJson.title} />
    <Box>
      <div
        dangerouslySetInnerHTML={{
          __html: data.donateJson.content.childMarkdownRemark.html,
        }}
      />
    </Box>
  </Layout>
);

Donate.propTypes = {
  data: PropTypes.object.isRequired,
};

export default Donate;

export const query = graphql`
  query DonateQuery {
    donateJson {
      title
      content {
        childMarkdownRemark {
          html
        }
      }
    }
  }
`;
