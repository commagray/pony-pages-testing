## Give us some ~~shekels~~ bits!
Coma Grayce maintain the main server and domains.

```
BTC: bc1qjjx643z38d80auy2n9zp0fwumvajjp5093rgzh
BCH: qqvj4p0ac84jd93mfsam4ucv9djuf0ewsvrwsp2r55
ETH: 0xA55036b434f778b1a128C7fb734f23356328903f
ETC: 0x4DB7d6347c6675baC209D41e27054a7509Da56c2
DASH: Xbrg3bgVBriquxXAYqfSpssZb4zhFeqrRN
ZEC: t1SdbxRdEMcKiFQfN9eMr7cfffgsgj3WqCT
LTC: LKURFNkjzQRUFNwcTQfhppyVvAHyjziidq
```

Andrew Morgan maintain the PeerTube server.

```
BTC: 1784zG6jxnp1FVbrk8GoJwrzradxLdyYeX
ZEC: t1bQ5gsdFcsTQCeUeD7vAfj27Mf2mrUGqtu
```
