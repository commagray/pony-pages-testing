## Public use
Public use services are for everyone and usually reachable without any email or captcha verification from user. We are focused on maintaining them as main services for a long time.

### Matrix
Our server originally started as a [Matrix](https://matrix.org) instance — a protocol for secure and federative communications. It binded to the canonical domain, so you can just type «mle.party» in your favorite client in a Home Server field and register an account on it. We recommend to use our [Riot](https://riot.im) instance on [chat.mle.party](https://chat.mle.party) to have all of nice features we provide.

### Shadowsocks
A little bit later we launched a [Shadowsocks](https://shadowsocks.org) server to have encrypted and mobile-friendly proxy. Everything is easy with it: just pick a [client](https://shadowsocks.org/en/download/clients.html) for your platform and paste [this link](ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTpib29raG9yc2VAbWxlLnBhcnR5OjgzODgK#MLE) in it. Or type all of the fields by hoof using the information below:

```
Name: MLE
Server: mle.party
Port: 8388
Password: bookhorse
Encryption: CHACHA20-IETF-POLY1305
```

### Searx
We can't trust Google and their spying tricks (but still use Google Fonts, sorry). One of the many problems is data mining using Google Search, wrapping you into a «search bubble» to promote related ads. We can't recommend any of the nonlibre search engines because they can't guarantee that they aren't spying on you. Because of that, we host a [Searx](https://asciimoo.github.io/searx) instance as a «better than nothing» option on [sunbutt.faith](https://sunbutt.faith).

### GitLab
Don't let companies rule your source code either. GitHub is a nonlibre service and Microsoft bought it some time ago making it even worse. Of course, self-hosted [GitLab](https://gitlab.com) is bloated and not full featured as their enterprise version, but it's an easy to use solution with great integrations and we use it on [moonbutt.science](https://moonbutt.science). Previously we used Gitea which isn't mature for our use cases yet.

### Pleroma
As you may have noticed earlier, we are pro-federation folks and trying to build a decentralized and useful network. As for social networks, there is the ActivityPub protocol and many good platforms built on it like [Pleroma](https://pleroma.social) which is a Twitter-like microblog. We host an instance of it at [ministry.moonbutt.science](https://ministry.moonbutt.science). There are many friends you can find!

### PeerTube
A lot of good creative content in our fandom gets released on YouTube as a convenient place to post it. But it's dangerous for the content itself, as it can and does get copyright striked by Hasbro and other companies, because our dear pastel-colored horses «belong» to Hasbro and they don't like pony content not by them. For archiving purposes we host a [PeerTube](https://joinpeertube.org) instance on [vault.mle.party](https://vault.mle.party). Feel free to upload your content on it.

### Write Freely
Continuing about decentralized networks, it will be useful to have a blog for longreads in addition. Medium is nonlibre and privacy nonfriendly, Wordpress is bloated and has problems with security, and both of them don't have a federation support by default. The author of nonlibre Write.as service made it libre as [Write Freely](https://writefreely.org) and our instance hosted on [gospel.sunbutt.faith](https://gospel.sunbutt.faith).

### 0x0
And if you want to share some file or URL as fast as possible you can use our [0x0](https://github.com/lachs0r/0x0) instance on [wasteland.mle.party](https://wasteland.mle.party).

## Legacy use
Legacy use services are mainly for bridging our Matrix rooms to old, but popular in their days protocols. We are looking on them sometimes, but they will stay as is and wouldn't get much support.

### IRC
We host a [modern IRC server](https://oragono.io) which can be used as an anonymous and blazing fast gateway for connecting to Matrix rooms, in the same way it can be used as a general communications tool (but remember that IRC doesn't provide any end-to-end encryption). You can get into [irc.mle.party](ircs://irc.mle.party:6697) using your [desktop](http://xchat.org) (or [web](https://kiwiirc.com)) client.

### XMPP
Also there is a [modern XMPP server](https://prosody.im/) with a lot of useful extensions turned on. It could be a good alternative to our Matrix server if you want mature clients for your desktop (and don't forget to turn on OMEMO encryption in your conversations). It binded to all of three of our domains, so pick one of them and register through your favorite client.

### Email
Please contact Coma Grayce to get your address in one of our domain zones.

## Private use
Private use services are secondary and could be dropped in any time. Some functinaly can't be reachable without help of one of our maintainers.

### TomeNET
Also, do you know many online multiplayer roguelikes? We don't. [TomeNET](https://tomenet.eu) is one of the active ones, and we are very happy to say that we host an instance of it. So, if you get bored of the official servers someday, come to [dungeons.mle.party](https://tomenet.eu/meta.php) through your client and say hi to us on our [IRC channel](ircs://irc.mle.party:6697/tomenet).

### Mumble
About VoIP: all of the market is currently captured by Discord and Skype, these services don't respect your privacy. Unfortunately, there is no libre, mature alternative for both desktop and mobile usage. In a «better than nothing» option we host a [Mumble](https://wiki.mumble.info) instance on the canonical domain.