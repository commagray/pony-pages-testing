## The Team
Maintainers are [Coma Grayce](mailto:commagray@sunbutt.faith)<sup>([PGP](https://mle.party/pubkey/commagray.txt))</sup>, [Andrew Morgan](mailto:andrew@amorgan.xyz)<sup>([PGP](https://mle.party/pubkey/anoa.txt))</sup>, and [Kolonsky](mailto:Kolonsky@mle.party).

## Chat stuff
Join us in [Matrix](https://matrix.to/#/+ministry:mle.party), [IRC](ircs://irc.mle.party:6697/general), [Discord](https://discord.gg/2faPsBu), and [Telegram](https://t.me/telemle).

## Backstory
**TL;DR**: MLE — or My Little Equestria — is a mainly Russian-speaking group of fallen souls, who was unlucky to stumble on a series about our dear pastel-colored horses. For our convenience, we provide hosting for some self-hosted programs, which are freely available for everyone's using.

Everything started in 2016, in a group chat in a nonlibre Russian social network called VKontakte. It was a conference about My Little Pony with a bot in them titled Twichat. Both of them was maintained by Marshmallow. They had a very good traffic: everyday drama, roleplay and newcomers. But as time went, there was talk about poor VKontakte functionality and finding a new home.

In 2017 was a split-up in Twichat and a part of members moved to their own conference, now in Telegram. There was a couple of iterations of that conference, but none of them became a thing. There was a period of silence. Several attempts to use IRC, XMPP and even a mailing list — none of them was successful because of lack of features. The choice fell on the Matrix protocol and a room on matrix.org server.

In 2018 we still didn't have a name, but a room and a community in it was pretty stable. In the middle of the year we got a name — My Little Equestria, or just MLE. A little bit later we bought a domain and VPS, and started our own Matrix server. There was a split-up again and part of members moved to Discord in their own guild titled The Core, which is still closely related to MLE and even bridged to it.

By now we are building a little infrastructure of libre software and looking for new friends.
