const path = require('path');

module.exports = {
  siteTitle: `My Little Equestria`,
  siteTitleShort: `MLE`,
  siteDescription: `You know, ponies. We are all doomed.`,
  siteUrl: `https://mle.party`,
  themeColor: `#fff`,
  backgroundColor: `#000`,
  pathPrefix: null,
  logo: path.resolve(__dirname, 'src/images/logo.svg')
};
